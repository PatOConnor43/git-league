# Git League
A fun way to stat track your git commits in a federated way.

## Architecture
There are two main paths for Git League. Gathering Commits and Querying Commits.
Gathering commits is done by the python client. This client harvests commit data
as a commit hook and sends it to a lambda for storage. This is shown below:

![Gathering Commits](http://www.plantuml.com/plantuml/proxy?src=https://gitlab.com/PatOConnor43/git-league/raw/master/plantuml/gather_commits.uml)

Querying Commits is done by the Git League Dashboard. The dashboard is responsible
for querying a presenting the data in an aggregated way. This sequence is shown below:

![Querying Commits](http://www.plantuml.com/plantuml/proxy?src=https://gitlab.com/PatOConnor43/git-league/raw/master/plantuml/querying_commits.uml)

## Development Tools
- [OpenApiTools-generator](https://github.com/OpenAPITools/openapi-generator)
  - Used to generate client/server from an OpenApi spec
  - `docker run --rm -v ${PWD}:/local openapitools/openapi-generator-cli generate -i /local/swagger.json -g python -o /local/client/gitleague/gen/python`
- [swagger-ui](https://github.com/swagger-api/swagger-ui)
  - Used to view the spec and try it out in the browser with real-time updates.
  - Starts on port 8123 as part of the docker-compose.
- [APISprout](https://github.com/danielgtaylor/apisprout)
  - Immidiately starts a mock server that uses the examples in the spec or auto-generates responses based on the spec.
  - Starts on port 8000 as part of the docker-compose.

## Local development
Local development is done using localstack to simulate an AWS-like environment.
Start the docker compose with `docker-compose up`. 
> There may be a voluming issue on MacOS. You can use `TMPDIR=/private$TMPDIR docker-compose up`
to get around this.

To create the necessary resources in localstack run `scripts/setup.sh`. After
running the script you should see an example curl request hit the hello_world
lambda to verify that everything is working as expected.

The Python client can be tested against the localstack implementation which
will run against real lambda functions, or against a mock server that is
exposed on port 8000 when the docker-compose is started.

