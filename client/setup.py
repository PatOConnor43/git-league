#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages
from os import path
from io import open

with open(path.join(path.abspath(path.dirname(__file__)), 'README.md'),
          encoding='utf-8') as f:
    long_description = f.read()

setup(
    # Basic project information
    name='git-league',
    version='0.0.1',
    # Authorship and online reference
    author='Pat O\'Connor',
    author_email='patoconnor43@gmail.com',
    url='https://gitlab.com/PatOConnor43/git-league',
    # Detailled description
    description='Python3 client to gather commit meta information',
    long_description=long_description,
    long_description_content_type='text/markdown',
    keywords='git development',
    classifiers=[
        'Programming Language :: Python',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    # Package configuration
    packages=find_packages(exclude=("tests",)),
    include_package_data=True,
    python_requires=">=3.6",
    install_requires=[
        'click>=7.0'
        ],
    # Licensing and copyright
    license='MIT',
    entry_points={
        'console_scripts': ['git-league-client=gitleague.main:main'],
    }
)
